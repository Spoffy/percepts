from bottle import route, run, post, get, abort, request, BaseRequest
import vuforia
import base64
import io
#Workaround, else can't recieve big requests, e.g images.
BaseRequest.MEMFILE_MAX = 1024 * 10000 

@get('/images/<id>')
def getinfo(id):
	if not id:
		return abort(404, 'Resource ID required')
	
	json = vuforia.getData(id)
	return json
	
#	fake = {'title':'A banana', 'author':'Me', 'rating':'4.2','targetId':id,'comments':['Comment one', 'Comment two']}
#	return json.dumps(fake)

@post('/images')
def postphoto():
	result = vuforia.addData(request.forms)
	return result

@post('/images/<id>/comment')
def postcomment(id):
	return vuforia.addComment(id, request.forms)	

@post('/images/<id>/like')
def postlike(id):
	return vuforia.adjLikes(id, 1)

@post('/images/<id>/dislike')
def postDislike(id):
	return vuforia.adjLikes(id, -1)

from PIL import Image
import sys
import tempfile
@post('/test')
def testing():
	#Hacky solution to shrinking image sizes. Seriously. Must be a better way here.
	image = request.forms.get('imgData')
	dec = vuforia.decodeImage(image)
	io.open('Temp.jpg',mode='w+b').write(dec)
	im = Image.open('Temp.jpg')
	im.save('Small.jpg','JPEG',quality=94)
	buf = io.open('Small.jpg',mode='rb').read()
	encoded = base64.encodestring(buf)
	request.forms['imgData'] = encoded

	result = vuforia.addData(request.forms)
	print(result)
	return result

run(host='0.0.0.0',port='8080')
