import httplib
import signature
import json
import hashlib
import base64
import dbaccess

def get(target):
	date = signature.formattedDate()
	authCode = signature.sign('GET', date, '/targets/'+target)	
	
	conn = httplib.HTTPSConnection('vws.vuforia.com')	
	conn.request('GET', '/targets/'+target, headers={'Date':date, 'Authorization':authCode})
	resp = conn.getresponse().read()
	return json.loads(resp)

def post(encodedImage, name):
	date = signature.formattedDate()	

	content = json.dumps({'name':name, 'width':600.0, 'image':encodedImage})

	md5hash = hashlib.md5(content).hexdigest()
	signed = signature.sign('POST',md5=md5hash,contType='application/json', date=date, target='/targets')
	
	conn = httplib.HTTPSConnection('vws.vuforia.com', timeout=10)
	print('Debug')
	conn.request('POST', '/targets', content, headers={'Date':date,'Authorization':signed, 'Content-Type':'application/json'})
	resp = conn.getresponse().read()
	return json.loads(resp)

def decodeImage(imageData):
	append =  (4 - (len(imageData)%4))
	for x in range(0,append):
		imageData += '='
	return base64.b64decode(imageData)

#Takes data as a multidict.
def addData(data):
	encodedImage = data.get('imgData')	
	if not encodedImage:
		raise Exception('Missing Image in Form')
	result = post(encodedImage, hashlib.md5(encodedImage).hexdigest())
	if result['result_code'] == 'TargetCreated':
		result = dbaccess.addImage('Generic Title',result['target_id'],'Replace')
	return json.dumps(result)

def getData(targetId):
	result = dbaccess.getImage(targetId)
	return json.dumps(result) 
	
def addComment(targetId, data):
	comment = data.get('comment')
	result = dbaccess.addComment(targetId, comment)
	return json.dumps(result)

def adjLikes(targetId, direction):
	result = dbaccess.like(targetId, direction)
	return json.dumps(result)

