import httplib
import Signature
import json
import hashlib
import io
import base64

filez = io.open('shush.jpg', mode='rb')
bytez = filez.read()

encodad = base64.b64encode(bytez)

jsn = json.dumps({'name':'Target', 'width':320.0, 'image':encodad})

md5 = hashlib.md5(jsn).hexdigest()
date = Signature.formattedDate()

signed = Signature.sign('POST',md5=md5,contType='application/json', date=date, target='/targets')
print(signed)

conn = httplib.HTTPSConnection('vws.vuforia.com')
conn.set_debuglevel(1)
conn.request('POST', '/targets', jsn, headers={'Date':date,'Authorization':signed, 'Content-Type':'application/json'})
resp = conn.getresponse()

print(resp.read())
