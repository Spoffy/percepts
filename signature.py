import hmac
import hashlib
import base64

SECRET_KEY = '7444670b870363a36893f6a928adfcd06a62822e'
PROVISION_KEY = '6d6a6f1c09bfe4859fd1cf43c5e535226e6a9289' 

#MD5 hash defaults to hash of an empty string. Cont type defaults to empty string.
def sign(method, date, target, md5='d41d8cd98f00b204e9800998ecf8427e', contType=''):
	stringToSign = method + '\n' + md5 + '\n' + contType + '\n' + date + '\n' + target

	auth = hmac.new(SECRET_KEY, stringToSign, hashlib.sha1)
	digest = auth.hexdigest()

	#Refactor
	charString = ''
	for c in range(0,len(digest),2):
		charString += chr(int(digest[c:c+2],16))

	signature = base64.b64encode(charString)

	return 'VWS ' + PROVISION_KEY + ':' + signature 

from email.utils import formatdate
from datetime import datetime
from time import mktime

def formattedDate():
	now = datetime.now()
	stamp = mktime(now.timetuple())
	return formatdate(timeval= stamp, localtime = False, usegmt = True)
