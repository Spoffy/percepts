import io
import json
import httplib
import urllib
import base64
import dbaccess
import hashlib 
import sys
import dbaccess

STD_GET = '66cda3d823f6467483efa65d956ea927'
STD_PUT = 'first.jpg'

def post():
	jpgStream = io.open(STD_PUT,'rb')
	data = jpgStream.read()
	data = base64.standard_b64encode(data)
	
	print('Size = ' + str(sys.getsizeof(data)))
	print('SHA1 hash = ' + str(hashlib.sha1(data).hexdigest()))
	encoded = urllib.urlencode({'imgData':data})

	conn = httplib.HTTPConnection('localhost','8080')
	conn.request('POST', '/test', encoded, headers={'Content-Type':'application/x-www-form-urlencoded'})
	print(conn.getresponse().read())

def get():
	conn = httplib.HTTPConnection('localhost','8080')

	conn.request('GET', '/images/' + STD_GET)
	print(conn.getresponse().read())

def comment():
	conn = httplib.HTTPConnection('localhost','8080')

	encoded = urllib.urlencode({'comment':'The best container ever!'})
	conn.request('POST', '/images/' + STD_GET + '/comment', encoded, headers={'Content-Type':'application/x-www-form-urlencoded'})
	print(conn.getresponse().read())

def addshit():
	dbaccess.addImage('Safari', '171548eeadcd4d6b9e52c62585e11f18', 'Wow an animal')

def likeDislike():
	conn = httplib.HTTPConnection('localhost','8080')
	conn.request('POST', '/images/' + STD_GET + '/like')
	print(conn.getresponse().read())

import random
def randomComments():
	choices = ('Awesome pic!', 'Whoa.', 'Looks good', 'what is this?', 'can you see this?', 'whoa. EPIC.', 'Meh. Pretty dull', 'Interesting...', 'Reminds me of fred...', 'Not exciting enough', 'sweet app!')
	collection = dbaccess._connect()
	cursor = collection.find()
	xsum = 0
	for x in cursor:
		comments = x.get('comments')
		targetId = x.get('targetId')
		newcomments = []
		for y in comments:
			if y != 'Replace':
				newcomments.append(y)
			else:
				newcomments.append(choices[random.randint(0,len(choices)-1)])
		#print(newcomments)
		collection.update({'targetId':targetId}, {'$set':{'comments':newcomments}}, multi=True)
		print(collection.find_one({'targetId':targetId},{'comments':True}))

randomComments()
