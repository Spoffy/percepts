from pymongo import MongoClient
import pymongo

import time

#Connect to DB server, return the main image collection
def _connect():
	client = MongoClient()
	return client.imageDatabase.main

# Standardised return type, so we know what we're getting.
def _returnDict(targetId,success,comments=[],likes=None,title=None,author=None):
	return {'targetId':targetId, 'success':success, 'comments':comments, 'likes':likes, 'title':title, 'author':author}

def addImage(title, targetId, comment, author='Default'):
	collection = _connect()
	collection.insert({'title':title, 'targetId':targetId, 'comments':[comment], 'author':author, 'likes':0})
	return _returnDict(targetId,'true')

def getImage(targetId):
	collection = _connect()
	found = collection.find_one({'targetId':targetId}, {'_id':False}) 
	if found:
		return _returnDict(targetId,'true',found['comments'],found['likes'],found['title'],found['author'])
	
	return _returnDict(targetId,'false')

def addComment(targetId, comment):
	collection = _connect()
	data = getImage(targetId)
	if data:
		newComment = data['comments']+[comment]
		result = collection.update({'targetId':targetId}, {'$set':{'comments':newComment}}, multi=True)
		if result['updatedExisting']:
			return _returnDict(targetId, 'true')

	return _returnDict(targetId,'false')

def like(targetId,direction):
	collection = _connect()
	return collection.update({'targetId':targetId}, {'$inc':{'likes':direction}}, multi=True)
